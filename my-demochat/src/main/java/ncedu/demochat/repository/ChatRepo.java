package ncedu.demochat.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;


@Repository
public class ChatRepo extends JdbcDaoSupport {

    private DataSource dataSource;

    @Autowired
    public ChatRepo(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @PostConstruct
    private void init() {
        setDataSource(dataSource);

        getJdbcTemplate().execute("drop table if exists messages");
        //getJdbcTemplate().execute("drop table if exists Persons");
        //getJdbcTemplate().execute("insert into Persons (personid, lastname, firstname, address, city ) values (3, 'Jef', 'Rex', 'tver', 'tver')");
        getJdbcTemplate().execute("create table messages (id SERIAL, sender INT, text_ VARCHAR(4000), receiver INT)");
        getJdbcTemplate().execute("insert into messages (sender, receiver, text_) values (1,2,'Hello')");
        getJdbcTemplate().execute("insert into messages (sender, receiver, text_) values (2,1,'Hello')");
    }

//    public void addMessage(int personid, String  ln, String fn, String address, String city){
//
//        getJdbcTemplate().update("insert into Persons (personid, lastname, firstname, address, city) values (?,?,?)",
//                new Object[] {personid, ln, fn, address, city});
//    }

    public void addMessage(int sender, int receiver, String text) {

        //getJdbcTemplate().update("insert into messages (sender, receiver, text_) values (?,?,?)", new Object[] {sender, receiver, text});
    }

//    public Map<Integer, List<String>> getUserMessages(int receiver) {
//
//        return getJdbcTemplate().query("select sender, text_ from messages where receiver = ?",
//                new Object[]{receiver}, new ResultSetExtractor<Map<Integer, List<String>>>() {
//                    @Override
//                    public Map<Integer, List<String>> extractData(ResultSet resultSet)
//                            throws SQLException, DataAccessException {
//
//                        Map<Integer, List<String>> map = new HashMap<>();
//                        while (resultSet.next()) {
//                            int userId = resultSet.getInt(1);
//
//                            if (map.containsKey(userId)) {
//                                map.get(userId).add(resultSet.getString("text_"));
//                            } else {
//                                map.put(userId, new ArrayList<>());
//                                map.get(userId).add(resultSet.getString(2));
//                            }
//
//                        }
//                        return map;
//                    }
//                });
//
//    }

}
