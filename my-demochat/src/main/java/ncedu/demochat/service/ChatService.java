package ncedu.demochat.service;

import ncedu.demochat.dataasset.ChatPOJO;
import ncedu.demochat.dataasset.MessageDTO;
import ncedu.demochat.repository.ChatRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Service
public class ChatService {
    private Map<String, ChatPOJO> chats;

    @Autowired
    private ChatRepo chatRepo;

    public ChatService() {
        this.chats = new HashMap<>();
    }

    public ResponseEntity addMessage(MessageDTO messageDTO, String chat, String newmsg) throws SQLException {

        //chatRepo.addMessage(messageDTO.getSender(), messageDTO.getReceiver(), messageDTO.getText());

//        if(null == messageDTO.getName()
//                || null == messageDTO.getText()){
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Incomplete income data");
//        }
//
//        if(!"".equals(newmsg)){
//            ChatPOJO chatPOJO = chats.get(chat);
//            if(chatPOJO == null)
//                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Sorry, there is no such chat");
//
//            chatPOJO.changeMessage(messageDTO, newmsg);
//        }
//        else {
//            ChatPOJO chatPojo = getChatById(chat);
//            chatPojo.addMessage(messageDTO);
//        }

        return ResponseEntity.ok().build();
    }

    public ResponseEntity<String> getMessages(String chat){

        // Get Chat for read
        ChatPOJO chatPOJO = chats.get(chat);
        if(null == chatPOJO){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Chat not found");
        }


        return ResponseEntity.ok(chatPOJO.getMessages());
    }

    public ResponseEntity deleteMessage(String chat, MessageDTO messageDTO){
        ChatPOJO chatPOJO = chats.get(chat);

        if(chatPOJO == null)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Sorry, there is no such chat");

        chatPOJO.deleteMessage(messageDTO);
        return ResponseEntity.ok().build();
    }

//   public ResponseEntity changeMessage(String chat, MessageDTO msg, String newmsg){
//        ChatPOJO chatPOJO = chats.get(chat);
//
//        if(chatPOJO == null)
//            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Sorry, there is no such chat");
//
//        chatPOJO.changeMessage(msg, newmsg);
//
//        return ResponseEntity.ok().build();
//    }


    private ChatPOJO getChatById(String chat){

        ChatPOJO chatPOJO = chats.get(chat);
        if(null != chatPOJO){
            return chatPOJO;
        }

        ChatPOJO newChat = new ChatPOJO();
        chats.put(chat, newChat);
        return newChat;
    }
}
