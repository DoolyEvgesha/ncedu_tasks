package ncedu.demochat.dataasset;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ChatPOJO {
    private List<MessageDTO> messages;

    public ChatPOJO() {
        this.messages = new ArrayList<>();
    }

    public void addMessage(MessageDTO messageDTO){
        messageDTO.setTime(new SimpleDateFormat("hh:mm:ss").format(new Date()));
        messages.add(messageDTO);

    }

    public String getMessages(){
        StringBuilder result = new StringBuilder();
        for (MessageDTO x : messages){
            result.append(x.getTime())
                    .append(" ")
                    .append(x.getName())
                    .append(" >> ")
                    .append(x.getText())
                    .append("\n");
        }
        return result.toString();
    }

    public void deleteMessage(MessageDTO messageDTO){
        for(MessageDTO x : messages){
            if(messageDTO.getName().equals(x.getName()) && messageDTO.getText().equals(x.getText())) {
                messages.remove(x);
                break;
            }
        }
    }

    public void changeMessage(MessageDTO msg, String newmsg){
        for(MessageDTO x : messages){
            if(msg.getName().equals(x.getName()) && msg.getText().equals(x.getText())) {

                MessageDTO changedmsg = new MessageDTO();

                changedmsg.setTime(x.getTime());
                changedmsg.setName(x.getName());
                changedmsg.setText(newmsg);

                messages.set(messages.indexOf(x), changedmsg);
                break;
            }
        }

    }
}
