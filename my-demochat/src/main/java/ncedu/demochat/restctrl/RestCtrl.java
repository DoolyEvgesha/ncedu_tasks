package ncedu.demochat.restctrl;

import ncedu.demochat.dataasset.MessageDTO;
import ncedu.demochat.service.ChatService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;

@RestController
public class RestCtrl {

    @Autowired
    private ChatService chatService;

    private Logger logger = LoggerFactory.getLogger(RestCtrl.class);

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity addMessage(
            @RequestBody                            MessageDTO messageDTO,
            @RequestParam(defaultValue = "general") String     chat,
            @RequestParam(defaultValue = "")        String     newmsg)
    throws SQLException {

        //instead of System.out.println()
        //logger.error("smth is wrong\n");
        //logger.debug("chat {} newmsg {}\n", chat, newmsg);

        return chatService.addMessage(messageDTO, chat, newmsg);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<String> getMessages(
            @RequestParam(defaultValue = "general") String chat){

        return chatService.getMessages(chat);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity deleteMessage(
            @RequestBody                            MessageDTO messageDTO,
            @RequestParam(defaultValue = "general") String     chat){

        return chatService.deleteMessage(chat, messageDTO);
    }
//
//    @RequestMapping(method = RequestMethod.POST)
//    public ResponseEntity changeMessage(@RequestBody                            MessageDTO msg,
//                                        @RequestParam(defaultValue = "general") String     chat,
//                                        @RequestParam                           String     newmsg){
//        return chatService.changeMessage(chat, msg, newmsg);
//    }

}
