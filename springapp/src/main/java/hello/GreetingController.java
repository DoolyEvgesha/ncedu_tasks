package hello;

import java.math.BigInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.*;

@RestController
public class GreetingController {

        private static final String template = "Hello, %s!";
        private final AtomicLong counter = new AtomicLong();

        @RequestMapping(value = "/greeting", method = RequestMethod.GET)
        public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
            return new Greeting(counter.incrementAndGet(),
                    String.format(template, name));

        }

        @RequestMapping(value = "/getMode", method=RequestMethod.GET)
        public BigInteger getMode(@RequestParam(value="num") BigInteger number,
                                  @RequestParam(value="m")   BigInteger mode){
                return number.mod(mode);
        }

        @RequestMapping(value = "/modifyString", method = RequestMethod.POST)
        public String modifyString(@RequestParam String userName, @RequestBody String message){
                return message.replace("##user_name##", userName);
        }

//run by mvn spring-boot:run

}
//POST: /modifyString. params: userName, body: message,
// return: String - метод для подставления в строку имени юзера.
// Т.е. в параметрах отправляем имя пользователя, а в бади отправляем строку,
// внутри которой есть макрос (например ##user_name##), который нужно заменить на имя пользователя.

//Необходимо сделать RestController с двумя интерфейсами:
//GET: /getMode. params: number, mode.
// return number - метод для получения значения number по модулю mode. (использовать BigInteger)